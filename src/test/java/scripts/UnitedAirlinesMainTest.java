package scripts;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.UnitedAirlinesBasePage;
import utilities.Waiter;

public class UnitedAirlinesMainTest extends UnitedAirlinesBase {


    @BeforeMethod
    public void setPage(){
        unitedAirlinesBasePage = new UnitedAirlinesBasePage();
    }


    @Test(priority = 1, description = "Validate Main menu navigation items")
    public void validateMainMenuNavTabs(){
        String[] str = {"BOOK", "MY TRIPS", "TRAVEL INFO", "MILEAGEPLUS® PROGRAM", "DEALS", "HELP"};
        for (int i = 0; i < str.length; i++) {
            Assert.assertTrue(unitedAirlinesBasePage.headerTabs.get(i).isDisplayed());
            Assert.assertEquals(unitedAirlinesBasePage.headerTabs.get(i).getText(), str[i]);
    }}

    @Test(priority = 2, description = "Validate Book travel menu navigation items")
    public void validateBookTravelMenu(){
        String[] str = {"Book", "Flight status", "Check-in", "My trips"};
        for (int i = 0; i < str.length; i++) {
            Assert.assertTrue(unitedAirlinesBasePage.bookTravelTabs.get(i).isDisplayed());
            Assert.assertEquals(unitedAirlinesBasePage.bookTravelTabs.get(i).getText(), str[i]);

        }
    }


    @Test(priority = 3, description = "Validate Round-trip and One-way radio buttons")
    public void validateroundTripButtonAndoneWayButtonButtons(){
        Assert.assertTrue(unitedAirlinesBasePage.roundTripDisplay.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.roundTripButton.isEnabled());
        Assert.assertTrue(unitedAirlinesBasePage.roundTripButton.isSelected());
        Assert.assertTrue(unitedAirlinesBasePage.oneWayDisplay.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.oneWayButton.isEnabled());
        Assert.assertFalse(unitedAirlinesBasePage.oneWayButton.isSelected());
        unitedAirlinesBasePage.oneWayButton.click();
        Assert.assertTrue(unitedAirlinesBasePage.oneWayButton.isSelected());
        Assert.assertFalse(unitedAirlinesBasePage.roundTripButton.isSelected());

    }


    @Test(priority = 4, description = "Validate Book with miles and Flexible dates checkboxes")
    public void validateBookAndFlexibleCheckboxes(){
        Assert.assertTrue(unitedAirlinesBasePage.bookWithMilesCheckbox.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.bookWithMilesCheckbox.isEnabled());
        Assert.assertFalse(unitedAirlinesBasePage.bookWithMilesCheckboxSelect.isSelected());

        Assert.assertTrue(unitedAirlinesBasePage.flexibleDatesCheckbox.isDisplayed());
        Assert.assertTrue(unitedAirlinesBasePage.flexibleDatesCheckbox.isEnabled());
        Assert.assertFalse(unitedAirlinesBasePage.flexibleDatesCheckboxSelect.isSelected());

        unitedAirlinesBasePage.bookWithMilesCheckbox.click();
        unitedAirlinesBasePage.flexibleDatesCheckbox.click();

        Assert.assertTrue(unitedAirlinesBasePage.bookWithMilesCheckboxSelect.isSelected());
        Assert.assertTrue(unitedAirlinesBasePage.flexibleDatesCheckboxSelect.isSelected());

        unitedAirlinesBasePage.bookWithMilesCheckbox.click();
        unitedAirlinesBasePage.flexibleDatesCheckbox.click();

        Assert.assertFalse(unitedAirlinesBasePage.bookWithMilesCheckboxSelect.isSelected());
        Assert.assertFalse(unitedAirlinesBasePage.flexibleDatesCheckboxSelect.isSelected());

    }

    @Test(priority = 5, description = "Validate One-way ticket search results from Chicago, IL, US (ORD) to Miami, FL, US (MIA)")
    public void validateTicketSearchResults(){

        unitedAirlinesBasePage.oneWayButton.click();
        unitedAirlinesBasePage.fromButton.click();
        unitedAirlinesBasePage.clearFromButton.click();
        unitedAirlinesBasePage.fromButton.click();
        unitedAirlinesBasePage.fromButton.sendKeys("Chicago, IL, US (ORD)");
        unitedAirlinesBasePage.toButton.sendKeys("Miami, FL, US (MIA)");
        unitedAirlinesBasePage.departDateButton.click();
        unitedAirlinesBasePage.previousMonthButton.click();
        unitedAirlinesBasePage.dateInput.click();
        unitedAirlinesBasePage.passengerButton.click();
        unitedAirlinesBasePage.addAnAdult.click();
        Waiter.pause(1);
        unitedAirlinesBasePage.selectClassButton.click();
        unitedAirlinesBasePage.selectBusinessOrFirstClass.click();
        unitedAirlinesBasePage.findFlightsButton.click();
        Assert.assertEquals(unitedAirlinesBasePage.departureDetails.getText(), "DEPART ON: February 28");



        
    }







}
